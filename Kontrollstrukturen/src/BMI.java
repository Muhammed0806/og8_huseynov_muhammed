import java.util.Scanner;

public class BMI {
	private static Scanner scan;
	public static void main(String[] args) {
		scan = new Scanner(System.in);
		System.out.println("Ihr Gewicht? (Ganzzahl)");
		double gewicht = scan.nextInt();

		System.out.println("Ihre Gro�e? (Ganzzahl)");
		double size = scan.nextInt();
		size = size/100;

		System.out.println("Ihr Geschlecht ? (F�r M: 1 oder f�r W: 0 )");
		double geschlecht = scan.nextInt();

		double myBMI;
		myBMI = gewicht / (size * size);

		if (geschlecht == 1) {
			if (myBMI > 0 && myBMI < 20) {
				System.out.println("Ihr BMI: " + myBMI + " = Sie sind untergewichtig");
			} else if (myBMI > 19 && myBMI < 25) {
				System.out.println("Ihr BMI: " + myBMI + " = Sie haben ein normales Gewicht");
			} else if (myBMI> 25) {
				System.out.println("Ihr BMI: " + myBMI + " = Sie sind uebergewicht");
			}
		} else if (geschlecht == 0) {
			if (myBMI > 0 && myBMI < 19) {
				System.out.println("Ihr BMI: " + myBMI + " = Sie sind untergewichtig");
			} else if (myBMI > 19 && myBMI < 24) {
				System.out.println("Ihr BMI: " + myBMI + " = Sie haben ein normales Gewicht");
			} else if (myBMI> 24) {
				System.out.println("Ihr BMI: " + myBMI + " = Sie sind uebergewicht");
			}
		}
	} // end of main
} // end of class BMI
