import java.util.Scanner;

public class Summe {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.println("Gib Wert ein");
		int air = scan.nextInt();
		int summe = 0;
		for (int i = 1; i < air; i++) {
			summe = summe + (2 * i);
		}
		System.out.println("Summe: " + summe);
	}

}