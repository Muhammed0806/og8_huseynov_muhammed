
public class Addon {

	private int id_Nummer;
	private String name;
	private double preis;
	private int bestand;
	
	public Addon(int id_Nummer,String name, double preis, int bestand) {
		super();
		this.id_Nummer = id_Nummer;
		this.name = name;
		this.preis = preis;
		this.bestand = bestand;
	}	
	
	public int getId_Nummer() {
		return id_Nummer;
	}

	public void setId_Nummer(int id_Nummer) {
		this.id_Nummer = id_Nummer;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPreis() {
		return preis;
	}

	public void setPreis(double preis) {
		this.preis = preis;
	}

	public int getBestand() {
		return bestand;
	}

	public void setBestand(int bestand) {
		this.bestand = bestand;
	}
	
	public void verbrauch() {		
	}
	
	public void kauf() {
	}
	
	
}
