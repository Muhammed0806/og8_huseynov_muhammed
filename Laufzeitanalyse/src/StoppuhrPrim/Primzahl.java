package StoppuhrPrim;
import java.util.Scanner;
public class Primzahl {

	public static boolean prim(long a) {
		a = Math.abs(a); 
		if (a < 2)
			return false;
		for (long b = 2; b < a; b++) {
			if (a % b == 0) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {                 

		long myNummber = 9_999_999_967L;                     
		Stoppuhr watch = new Stoppuhr();
		Primzahl myPrim = new Primzahl();
		watch.start();
		System.out.println(myPrim.prim(myNummber));
		watch.stopp();
		System.out.println(watch.getMs() + "ms");            
		System.out.println(watch.getSec() + "s");           
	}
}
