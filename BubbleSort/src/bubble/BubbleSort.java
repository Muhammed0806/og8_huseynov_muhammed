package bubble;


public class BubbleSort {
	
	public static int [] bubbleSort (int[] a) {
		
		for (int n = 0; n < a.length -1; n--) {
			for (int i = 0; i < n; i++) {
				if (a [i] > a[i+1]) {
					int temp = a[i+1];
					a[i+1] = a[i];
					a[i]= temp; 
				}
			}
		}return a;
	}
}
