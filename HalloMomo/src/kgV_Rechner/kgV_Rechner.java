package kgV_Rechner;

import java.util.Scanner;

public class kgV_Rechner {

	public static int kgV(int a, int b) {

		int zahl1 = a;
        int zahl2 = b;
		
		if (zahl1 <= 0 || zahl2 <= 0)			//wenn eine der beiden Zahlen 0 ist oder negativ
			return 0;

		while (a != b) {						//pr�ft ob die Zahlen gleich sind
			if (a < b)
				a += zahl1;
			else
				b += zahl2;
		}
		return a;
	}

	public static void main(String[] args) {
		System.out.println("Geben Sie ihre 1. Zahl ein");
		Scanner scan = new Scanner(System.in);
		int zahl1 = scan.nextInt();

		System.out.println("Geben Sie ihre 2. Zahl ein");
		Scanner sc = new Scanner(System.in);
		int zahl2 = sc.nextInt();

		System.out.println("kgV = " + kgV(zahl1, zahl2));
	}
}
