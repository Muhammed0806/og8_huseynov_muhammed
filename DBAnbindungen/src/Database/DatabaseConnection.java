package Database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseConnection {
	
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/keks";
	private String user = "root";
	private String password = "";
	
	public boolean InsertKekse (String bezeichnung, String sorte) {
		String sql = "INSERT INTO t_kekse (bezeichnung, sorte) VALUES ('" + bezeichnung + "', '" + sorte + "')";
		
		try {
			//JDBC-Treiber Laden
			Class.forName(driver);
			
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		DatabaseConnection dbc = new DatabaseConnection();
		dbc.InsertKekse("Butterkeks", "Vieell Butter");
	}
}
