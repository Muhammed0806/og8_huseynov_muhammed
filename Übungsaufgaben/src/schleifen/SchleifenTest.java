package schleifen;

import java.util.Scanner;

public class SchleifenTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int anzahl;
		System.out.println("Bitte geben Sie eine Zahl ein");
		anzahl = scan.nextInt();

		
		//Pluszeichen generieren
		for (int i = 0; i < anzahl; i++) {
			System.out.print('+');	
		}
		
		//Minuszeichen generieren
		int i = 0;
		
		while( i < anzahl) {
			System.out.println("-");
			i++;
		}
		
		//Malzeichen generieren
		i = 0;
		do {
			System.out.println("*");
			i++;
		}while ( i < anzahl);
		
	}

}
