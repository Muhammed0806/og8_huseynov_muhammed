package taschenrechner_v0;

public class Taschenrechner {

	public double add(double zahl1, double zahl2) {
		return zahl1 + zahl2;
	}
	
	public double sub(double zahl1, double zahl2){
		return zahl1 - zahl2;
	}
	
	public double mul(double zahl1, double zahl2){
		return zahl1 * zahl2;
	}

	public double div(double zahl1, double zahl2){
		return zahl1 / zahl2;
	}
	
}
