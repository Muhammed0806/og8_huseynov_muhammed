package Stoppuhr;

public class Stoppuhr {
	
	//Attribute
	private long startZeit;
	private long endZeit;
	
	//Methoden
	public void start() {
		this.startZeit = System.currentTimeMillis();
	}
	
	public void stopp() {
		this.endZeit = System.currentTimeMillis();
	}
	
	public void reset() {
		this.startZeit = 0;
		this.endZeit = 0;
	}
	
	public long getMs() {
		return endZeit - startZeit;
	}
	
	public long getSec() {
		return Math.round((endZeit - startZeit) / 1000);
	}
	

}