package Stoppuhr;
import java.util.Scanner;
public class Primzahl {

	public static boolean prim(long a) {                      // Erstelle meine Primzahlformel
		boolean c = true;

		if (a < 1) {                                         // Bedingungen
			c = false;
			return c;
		} else
			for (long b = a - 1; b > 1; b--) {

				if (a % b == 0) {
					c = false;
				}
			}
		return c;
	
	}

	public static void main(String[] args) {                 // Main Methode enth�lt die abfrage bis wohin das Programm "z�hlen" soll

		Scanner scan = new Scanner(System.in);
		System.out.println("Gib deine Zahl ein");            // Frage bei der Konsole
		long meineZahl = scan.nextLong();                      // Zahl wird als ein Int eingelesen

		for (long i = 0; i < meineZahl; i++) {                // For Schleife wird von 0 bis meineZahl (Die Zahl die eingelesen wird) durch Z�hlen

			if (prim(i) == true)                             //Pr�ft ob die Zahl die gerade von For loop gez�hlt wird (i++) auch eine Primzahl ist
				System.out.println(i);                       //und gibt die Primzahlen aus
		}

	}
}
