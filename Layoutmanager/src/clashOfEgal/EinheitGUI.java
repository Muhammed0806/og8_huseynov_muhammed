package clashOfEgal;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class EinheitGUI extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EinheitGUI frame = new EinheitGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public EinheitGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 300, 596);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		try {
			Font supercellFont = Font.createFont(Font.TRUETYPE_FONT,new File("Supercell-magic-webfont.ttf")).deriveFont(15f);		//Erstelle die Funktionalitšt meiner Supersell Fonts
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT,new File("Supercell-magic-webfont.ttf")));
			
		} catch (IOException | FontFormatException e) {
			// TODO: handle exception
		}
		
		JPanel northPanel = new JPanel();
		contentPane.add(northPanel, BorderLayout.NORTH);
		
		JLabel lbl_Titel = new JLabel("Clash of Egal");
		lbl_Titel.setFont(new Font("Dialog", Font.PLAIN, 30));
		northPanel.add(lbl_Titel);
		
		JPanel centerPanel = new JPanel();
		contentPane.add(centerPanel, BorderLayout.CENTER);
		centerPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		JLabel lbl_Bild = new JLabel("");
		lbl_Bild.setHorizontalAlignment(SwingConstants.CENTER);
		centerPanel.add(lbl_Bild);
		
		JLabel lbl_Name = new JLabel("");
		lbl_Name.setFont(new Font("Arial", Font.PLAIN, 20));
		lbl_Name.setHorizontalAlignment(SwingConstants.CENTER);
		centerPanel.add(lbl_Name);
		
		JLabel lbl_Cost = new JLabel("");
		lbl_Cost.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_Cost.setIcon(new ImageIcon("./src/clashOfEgal/Gold.png"));
		centerPanel.add(lbl_Cost);
		
		JButton btn_Buy = new JButton("Buy");
		centerPanel.add(btn_Buy);
		
		
		JPanel eastPanel = new JPanel();
		contentPane.add(eastPanel, BorderLayout.EAST);
		JButton btn_Next = new JButton("\t\u2192");
		btn_Next.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				nextSquad();
			}
			public void nextSquad(){
				Einheit e = EinheitenSpeicher.next(); 
				lbl_Bild.setIcon(new ImageIcon(e.getBild()));
				lbl_Name.setText(e.getName());
				lbl_Cost.setText(e.getCost()+"");
			}
		});
		
		eastPanel.setLayout(new GridLayout(0, 1, 0, 0));
		btn_Next.setFont(new Font("Arial", Font.PLAIN, 20));
		eastPanel.add(btn_Next);
	}
}
