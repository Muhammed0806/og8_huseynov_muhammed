package clashOfEgal;
public class Einheit {
	
	private String name;
	private int cost;
	private String bild;
	
	public Einheit(String name, int cost, String bild) {
		this.name = name;
		this.cost = cost;
		this.bild = bild;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public String getBild() {
		return bild;
	}

	public void setBild(String bild) {
		this.bild = bild;
	}

	@Override
	public String toString() {
		return "Einheit [name=" + name + ", cost=" + cost + ", bild=" + bild + "]";
	}
	
}
