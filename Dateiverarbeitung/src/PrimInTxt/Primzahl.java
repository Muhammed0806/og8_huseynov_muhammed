package PrimInTxt;

import java.io.File;
import java.util.Arrays;

public class Primzahl {
	 public static boolean[] prim(int a) {
	       
		    boolean[] myprim = new boolean[a + 1];
	        Arrays.fill( myprim, Boolean.TRUE); 
	        
	        myprim[0] = false; 
	        myprim[1] = false; 
	        
	        for (int i = 2; i <= a; i++) {
	            int counter = i;
	            if ( myprim[counter]) {
	            	counter += i;
	                while (counter <= a) {
	                	 myprim[counter] = false;
	                	 counter += i;
	                }
	            }
	        }
	        return  myprim;
	   }
	 public static void main(String[] args) {
			File file = new File("prim.txt");;
			DateiVerwalterPrim dv = new DateiVerwalterPrim(file);
			
			 boolean[] primzahlen = prim(1000000);
		    
			 for (int i = 0; i < primzahlen.length; i++) {
		         if (primzahlen[i]) {
		             System.out.println(i);
		             dv.schreibe(i); 
		         }
		     }
			dv.lesen();
		}
}