package PrimInTxt;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class DateiVerwalterPrim {
	private File file;
	
	public DateiVerwalterPrim(File file) {
		this.file = file;
	}
	
	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader (fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
		} catch (Exception e) {
			System.out.println("File nicht Vorhanden");
			e.printStackTrace();
		}
	}
	
	public void schreibe(int counter) {
		try {
			FileWriter fw = new FileWriter(this.file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(counter);
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (Exception e) {
			System.out.println("File nicht Vorhanden");
			e.printStackTrace();
		}
	}
}
