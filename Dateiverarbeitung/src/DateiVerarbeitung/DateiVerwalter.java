package DateiVerarbeitung;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class DateiVerwalter {
	private File file;
	
	public DateiVerwalter(File file) {
		this.file = file;
	}
	
	public void lesen() {
		try {
			FileReader fr = new FileReader(this.file);
			BufferedReader br = new BufferedReader (fr);
			String s;
			while ((s = br.readLine()) != null) {
				System.out.println(s);
			}
		} catch (Exception e) {
			System.out.println("File nicht Vorhanden");
			e.printStackTrace();
		}
	}
	
	public void schreibe(String s) {
		try {
			FileWriter fw = new FileWriter(this.file);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(s);
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (Exception e) {
			System.out.println("File nicht Vorhanden");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		File file = new File ("OG8.txt");
		DateiVerwalter dv = new DateiVerwalter(file);
		//dv.schreibe("Momo");
		dv.lesen();
	}
}
