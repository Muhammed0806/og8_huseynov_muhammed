package christMess;

import java.util.*;

public class Kind{
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	//Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode

	
	private String nachnamen;
	private String vornamen;
	private String gebdatum;
	private String wohnort;
	private String bravheitsgrad;

	public Kind() {
		super();
	}

	public Kind(String nachnamen, String vornamen, String gebdatum, String wohnort, String bravheitsgrad) {
		super();
		this.nachnamen = nachnamen;
		this.vornamen = vornamen;
		this.gebdatum = gebdatum;
		this.wohnort = wohnort;
		this.bravheitsgrad = bravheitsgrad;
	}
	
	public String getNachnamen() {
		return nachnamen;
	}

	public void setNachnamen(String nachnamen) {
		this.nachnamen = nachnamen;
	}

	public String getVornamen() {
		return vornamen;
	}

	public void setVornamen(String vornamen) {
		this.vornamen = vornamen;
	}

	public String getGebdatum() {
		return gebdatum;
	}

	public void setGebdatum(String gebdatum) {
		this.gebdatum = gebdatum;
	}

	public String getWohnort() {
		return wohnort;
	}

	public void setWohnort(String wohnort) {
		this.wohnort = wohnort;
	}

	public String getBravheitsgrad() {
		return bravheitsgrad;
	}

	public void setBravheitsgrad(String bravheitsgrad) {
		this.bravheitsgrad = bravheitsgrad;
	}

	
	@Override
	public String toString() {
		return "Kind [nachnamen=" + nachnamen + ", vornamen=" + vornamen + ", gebdatum=" + gebdatum + ", wohnort="
				+ wohnort + ", bravheitsgrad=" + bravheitsgrad + "]";
	}
}	