package listenGenerator;

import java.util.Random;
import stoppUhr.Stoppuhr;
import suchAlgorithmen.BinaereSuche;
import suchAlgorithmen.LineareSuche;

public class SortedListGenerator {

	public static long[] getSortedList(int laenge) {
		Random rand = new Random(111111);
		long[] zahlenliste = new long[laenge];
		long naechsteZahl = 0;

		for (int i = 0; i < laenge; i++) {
			naechsteZahl += rand.nextInt(3) + 1;
			zahlenliste[i] = naechsteZahl;
		}

		return zahlenliste;
	}

	public static void main(String[] args) {
		final int ANZAHL = 19_000_000; // 20.000.000

		long[] a = getSortedList(ANZAHL);
		long gesuchteZahl = a[ANZAHL - 1];

		long time = System.currentTimeMillis();

		LineareSuche suche = new LineareSuche();
		System.out.println("Lineare Suche");
		System.out.println(suche.suche(a, gesuchteZahl));
		
		System.out.println(System.currentTimeMillis() - time + "ms");

		//for (long x : a) {
	    // 	System.out.println(x);
		//}
		
		Stoppuhr watch = new Stoppuhr();
		BinaereSuche search = new BinaereSuche();
		System.out.println("Bin�re Suche");
		watch.start();
		
		System.out.println(search.suche(a, gesuchteZahl));
		
		watch.stopp();
		System.out.println(watch.getMs() + "ms");            
		
	}
}