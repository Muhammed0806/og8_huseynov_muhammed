package suchAlgorithmen;

public interface ISuchalgorithmus {

	// Konstante f�r nicht im Array enthalten
	int NICHT_GEFUNDEN = -1;

	// gibt die Stelle im Array wieder, wo sich die gesuchteZahl befindet oder -1
	int suche(long[] zahlen, long gesuchteZahl);

	// gibt die Anzahl der Vergleiche aus
	int getVersuche(long[] zahlen, long gesuchteZahl);
}
