package stoppUhr;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class StoppUhrGUI extends JFrame {

	private JPanel contentPane;
	private Stoppuhr timer = new Stoppuhr();
	private JLabel lblAnzeige;
	private long time;
	private boolean running = false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		StoppUhrGUI s = new StoppUhrGUI();
		s.setVisible(true);
		s.run();
	}

	/**
	 * Create the frame.
	 */
	public StoppUhrGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(0, 1, 0, 0));
		
		lblAnzeige = new JLabel("Startzeit:");
		panel.add(lblAnzeige);
		
		JButton btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				running = true;
				timer.start();
				time = System.currentTimeMillis();
				lblAnzeige.setText(time+"");
			}
		});
		panel.add(btnStart);
		
		JButton btnStopp = new JButton("Stopp");
		btnStopp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				running = false;
				timer.stopp();
				lblAnzeige.setText(timer.getSec() + "s");
			}
		});
		panel.add(btnStopp);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				timer.reset();
				lblAnzeige.setText("Startzeit:");
			}
		});
		panel.add(btnReset);
	}

	public void run(){
		while(true)
			if(running)
				lblAnzeige.setText((time - System.currentTimeMillis()) *-1+ "ms = " + (Math.round((time - System.currentTimeMillis()) *-1/1000.0))+ "s" );
	}
	
}
