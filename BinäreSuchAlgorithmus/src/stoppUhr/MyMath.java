package stoppUhr;

public class MyMath {

	public static boolean isPrim(long zahl){
		if(zahl < 2) return false;
		for(long i = 2; i <= zahl/2; i++){
			if(zahl % i == 0) return false;
		}
		return true;
	}
}
