package rekursion;


public class MiniMath {
	
	/**
	 * Berechnet die Fakult�t einer Zahl (n!)
	 * @param n - Angabe der Zahl
	 * @return n!
	 */
	public static int berechneFakultaet(int n){
		int erg= 1;
		for (int i = 1; i <= n; i++) {
			erg*= i;
		}		
		return erg;
	}
	
	/**
	 * Berechnet die 2er-Potenz einer gegebenen Zahl
	 * @param n - Angabe der Potenz (max. 31 sonst Integeroverflow)
	 * @return 2^n 
	 */
	public static int berechneZweiHoch(int n){
		
		if (n<31) {
			if (n==0) {
				return 1;
			}else {
				System.out.println("Die eingegeben Zahl ist gr��er als 31 und sorgt f�r ein Integeroverflow ");
			}
		}return 2*berechneZweiHoch(n-1);
		/*
		double air = Math.pow(2, n);
		int e = (int) air;
		return e;
		*/
	}
	
	/**
	 * Die Methode berechnet die Summe der Zahlen von 
	 * 1 bis n (also 1+2+...+(n-1)+n)
	 * @param n - ober Grenze der Aufsummierung
	 * @return Summe der Zahlen 1+2+...+(n-1)+n
	 */
	public static int berechneSumme(int n){
		int erg = 0;
		for (int i = 1; i <= n; i++) {
			erg += i;
		}
		return erg;
	}
	
}

