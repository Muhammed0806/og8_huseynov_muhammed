
public class Leiter extends Spieler {
	private String name;
	private double rabatt;

	public Leiter(String mannschaftsname, double rabatt, String name) {
		super();
		this.name = name;
		this.rabatt = rabatt;
	}

	public String getName() {
		return name;
	}

	public void setName(String mannschaftsname) {
		this.name = mannschaftsname;
	}

	public double getRabatt() {
		return rabatt;
	}

	public void setRabatt(double rabatt) {
		this.rabatt = rabatt;
	}

}
