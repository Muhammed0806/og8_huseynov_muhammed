import java.util.Arrays;

public class Trainer extends Mitglied {
	private Mannschaft[] team;
	private char lizenzClass;
	private double aufwand;

	public Trainer() {

	}

	public Trainer(char lizenzClass, double aufwand) {
		super();
		this.lizenzClass = lizenzClass;
		this.aufwand = aufwand;
		this.team = new Mannschaft[1];
	}

	public char getLizenzClass() {
		return lizenzClass;
	}

	public void setLizenzClass(char lizenzClass) {
		this.lizenzClass = lizenzClass;
	}

	public double getAufwand() {
		return aufwand;
	}

	public void setAufwand(double aufwand) {
		this.aufwand = aufwand;
	}

	public Mannschaft[] getTeam() {
		return team;
	}

	public void setTeam(Mannschaft[] team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "Tranier [Mannschaft =" + Arrays.toString(team) + ", Lizenz Klasse =" + lizenzClass
				+ ", aufwandschaedigung =" + aufwand + "]";
	}

}
