public class Schiedsrichter extends Mitglied {
	private int gepfiffeneSpiele;

	public Schiedsrichter(int gepfiffeneSpiele) {
		super();
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

	public int getGepfiffeneSpiele() {
		return gepfiffeneSpiele;
	}

	public void setGepfiffeneSpiele(int gepfiffeneSpiele) {
		this.gepfiffeneSpiele = gepfiffeneSpiele;
	}

}
