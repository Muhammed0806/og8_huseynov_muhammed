
public class Mitglied {
	
	//Attribute
	private String name;
	private long teleNr;
	private boolean jahresAboGezahlt;

	// Konstruktoren
	public Mitglied() {

	}

	public Mitglied(String name, long teleNr, boolean jahresAboGezahlt) {
		this.name = name;
		this.teleNr = teleNr;
		this.jahresAboGezahlt = jahresAboGezahlt;

	}
	
	//Methoden
		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public long getTeleNr() {
			return teleNr;
		}

		public void setTeleNr(long teleNr) {
			this.teleNr = teleNr;
		}

		public boolean jahresAboGezahlt() {
			return jahresAboGezahlt;
		}

		public void setJahresAboGezahlt(boolean jahresAboGezahlt) {
			this.jahresAboGezahlt = jahresAboGezahlt;
		}

}