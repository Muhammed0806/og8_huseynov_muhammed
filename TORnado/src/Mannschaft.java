import java.util.Arrays;

public class Mannschaft {

	// Attribute
	private int anzahlSpiele;
	private Spieler[] spieler;
	private Trainer tranier;
	private String name;
	private String spielKlasse;

	// Methoden

	public String getName() {
		return name;
	}

	public Mannschaft() {

	}

	public Mannschaft(int anzahlSpiele, Trainer tranier, String name, String spielKlasse) {
		super();
		this.anzahlSpiele = anzahlSpiele;
		this.spieler = new Spieler[22];
		this.setTranier(tranier);
		this.name = name;
		this.spielKlasse = spielKlasse;

	}

	public void setName(String nameNeu) {
		name = nameNeu;
	}

	public String getSpielKlasse() {
		return spielKlasse;
	}

	public void setSpielKlasse(String spielKlasseNeu) {
		spielKlasse = spielKlasseNeu;
	}

	public Trainer getTranier() {
		return tranier;
	}

	public void setTranier(Trainer tranier) {
		this.tranier = tranier;
	}

	public Spieler[] getSpieler() {
		return spieler;
	}

	public void setSpieler(Spieler[] spieler) {
		this.spieler = spieler;
	}

	public int getAnzahlSpiele() {
		return anzahlSpiele;
	}

	public void setAnzahlSpiele(int anzahlSpiele) {
		this.anzahlSpiele = anzahlSpiele;
		anzahlSpiele++;
	}

	@Override
	public String toString() {
		return "Mannschaft [anzSpiele=" + anzahlSpiele + ", spieler=" + Arrays.toString(spieler) + ", tranier=" + tranier
				+ ", name=" + name + ", spielKlasse=" + spielKlasse + "]";
	}	
} 
