import java.util.Arrays;

public class Spiele {

	//  Attribute
	private Mannschaft[] team;
	private String spieltyp;
	private String spielDatum;
	private String spielErgebnis;

	//Methoden
	public Spiele(String spieltyp, String spielDatum, String spielErgebnis) {
		this.spieltyp = spieltyp;
		this.spielDatum = spielDatum;
		this.spielErgebnis = spielErgebnis;
		this.team = new Mannschaft[2];
	}

	public String getSpieltyp() {
		return spieltyp;
	}

	public void setSpieltyp(String spieltypNeu) {
		spieltyp = spieltypNeu;
	}

	public String getSpielDatum() {
		return spielDatum;
	}

	public void setSpielDatum(String spielDatumNeu) {
		spielDatum = spielDatumNeu;
	}

	public String getSpielErgebnis() {
		return spielErgebnis;
	}

	public void setSpielErgebnis(String spielErgebnisNew) {
		spielErgebnis = spielErgebnisNew;
	}

	public Mannschaft[] getTeam() {
		return team;
	}

	public void setTeam(Mannschaft[] team) {
		this.team = team;
	}

	@Override
	public String toString() {
		return "Spiele [team =" + Arrays.toString(team) + ", spieltyp=" + spieltyp + ", spielDatum="
				+ spielDatum + ", ergebnis=" + spielErgebnis + "]";
	}

}
