
import java.util.Arrays;

public class Spieler extends Mitglied {
	private Mannschaft[] mannschft;
	private int trikotNummer;
	private String spielposition;

	public Spieler() {
	}

	public Spieler(int trikotNummer, String spielposition) {
		super();
		this.trikotNummer = trikotNummer;
		this.spielposition = spielposition;
		this.mannschft = new Mannschaft[1];
	}

	public int getTrikotNummer() {
		return trikotNummer;
	}

	public void setTrikotNummer(int trikotNummer) {
		this.trikotNummer = trikotNummer;
	}

	public String getSpielposition() {
		return spielposition;
	}

	public void setSpielposition(String spielposition) {
		this.spielposition = spielposition;
	}

	public Mannschaft[] getMannschft() {
		return mannschft;
	}

	public void setMannschft(Mannschaft[] mannschft) {
		this.mannschft = mannschft;
	}

	@Override
	public String toString() {
		return "Spieler [mannschft=" + Arrays.toString(mannschft) + ", trikotnummer=" + trikotNummer
				+ ", spielposition=" + spielposition + "]";
	}

}
