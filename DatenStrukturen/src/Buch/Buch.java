package Buch;

public class Buch implements Comparable<Buch> {
	
	private String autor;
	private  String titel; 
	private String iSBN_Nummer;
	
	public Buch (String autor, String titel, String iSBN_Nummer) {
		this.autor = autor;
		this.titel = titel;
		this.iSBN_Nummer = iSBN_Nummer;
	}
	
	
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitel() {
		return titel;
	}

	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getiSBN_Nummer() {
		return iSBN_Nummer;
	}

	public void setiSBN_Nummer(String iSBN_Nummer) {
		this.iSBN_Nummer = iSBN_Nummer;
	}

	

	@Override
	public String toString() {
		return null;
	}

	@Override
	public boolean equals(Object o) {
		return true;
	}

	@Override
	public int compareTo(Buch aBuch) {
		return 0;
	}
	
}
