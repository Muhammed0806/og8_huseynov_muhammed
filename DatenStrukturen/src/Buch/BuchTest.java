package Buch;

public class BuchTest {

	public static void main(String[] args) {

		Buch b1 = new Buch("Richard Helm", "Design Patterns", "ISBN-10:0201633612");
		Buch b2 = new Buch("Robert C. Martin", "Clean Code", "ISBN-10:0132350882");
		Buch b3 = b1;
		
		System.out.println("b1:  "+b1);
		System.out.println("b2:  "+b2);
		System.out.println("b3:  "+b3);
		
		System.out.println("b1.equals(b2)-->"+b1.equals(b2));
		System.out.println("b1.equals(b3)-->"+b1.equals(b3));
		
		System.out.println("b1.compareTo(b3)-->"+b1.compareTo(b3));
		System.out.println("b1.compareTo(b2)-->"+b1.compareTo(b2));
		System.out.println("b2.compareTo(b1)-->"+b2.compareTo(b1));
	}

}